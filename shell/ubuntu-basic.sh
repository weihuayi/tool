#! /bin/bash
#
#
# 2020.03.27: update for ubuntu 20.04
# 2019.01.09: update for ubuntu 18.04




# 安装中文支持
apt -y install language-pack-zh-hans 

# 安装中文输入法
apt -y install fcitx fcitx-table fcitx-pinyin


# 远程登录工具openssh-server
apt -y install openssh-server

# 程序开发工具 
## fortran编译器gfortran 
apt -y install  gfortran fort77  
## 软件构建管理工具
apt -y install build-essential automake cmake cmake-qt-gui 
## python3 相关
apt -y install python3 python3-dev python3-pip 
apt -y install python3-rope python3-flake8
apt -y install python3-dbg 
apt -y install python3-tk
apt -y install pybind11-dev
apt -y install python3-importlib-metadata




# 编辑器
apt -y install vim
apt -y install xdotool 
apt -y install zathura

 
# 文档编译器texlive
apt -y install texlive texlive-xetex texlive-science texlive-latex-extra 
apt -y texlive-fonts-extra texlive-lang-cjk
apt -y install texlive-extra-utils
apt -y install latexmk

# 并行库openmpi
apt -y install openmpi-bin openmpi-common openmpi-doc libopenmpi-dev

# OpenGL
apt -y install libglut3-dev

# QT 
apt -y install qtcreator qt5-default 

# 打包解压工具rar和unrar
apt -y install rar unrar

# 图形处理软件gimp和inkscape
apt -y install gimp inkscape

# pdf阅读批阅工具xournal
apt -y install xournal

# 文档格式转换工具pandoc, 把markdown转化为各种格式
apt -y install pandoc 

# 下载工具电驴
apt -y install amule 

# google 的 Chromium-brower浏览器
#apt -y install chromium-browser

# gitbook 
apt -y install npm
npm -g install gitbook-cli


# 录屏软件 
apt -y install kazam
apt -y install recordmydesktop

# 系统管理软件
apt -y install gnome-nettool


# 科学计算专业软件
apt -y install libmumps-dev
apt -y install nvidia-cuda-dev
apt -y install libgmp-dev libmpfr-dev 
apt -y install libboost-dev libboost-thread-dev libboost-system-dev 
apt -y install metis
apt -y install zlib1g-dev libblas-dev liblapack-dev
apt -y install liblua5.3-dev

# snap 
snap install mathpix-snipping-tool
