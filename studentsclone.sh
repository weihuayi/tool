#! /bin/bash
#

if [ ! -d "~/git/students" ];then
    mkdir ~/git/students
fi

cd /home/why/git/students
git clone https://gitlab.com/whygroup/students/caohuihui-knowledge.git
git clone https://gitlab.com/whygroup/students/wangpengxiang-knowledge.git
git clone https://gitlab.com/whygroup/students/wangdong-knowledge.git
git clone https://gitlab.com/whygroup/students/wangxin-knowledge.git
git clone https://gitlab.com/whygroup/students/lichengxin-knowledge.git
git clone https://gitlab.com/whygroup/students/gongxin-knowledge.git
git clone https://gitlab.com/whygroup/students/huhandan-knowledge.git
git clone https://gitlab.com/whygroup/students/liumeiyun-knowledge.git
git clone https://gitlab.com/whygroup/students/liujun-knowledge.git
git clone https://gitlab.com/whygroup/students/tiantian-knowledge.git
git clone https://gitlab.com/whygroup/students/zhoutiejun-knowledge.git
git clone https://gitlab.com/whygroup/students/chenchunyu-knowledge.git
git clone https://gitlab.com/whygroup/students/liujianggang-knowledge.git
git clone https://gitlab.com/whygroup/students/liao-knowledge.git
