#! /bin/bash
#

cd /home/why/git/paper
git clone https://gitlab.com/whypaper/phase.git # 刘美云
git clone https://gitlab.com/whypaper/gsphase.git # 龚欣
git clone https://gitlab.com/whypaper/lerecovery.git # 扈瀚丹
git clone https://gitlab.com/whypaper/tetopt.git # 王鹏祥
git clone https://gitlab.com/whypaper/polymesh.git # 王栋
git clone https://gitlab.com/whypaper/quadadaptive.git # 陈春雨
git clone https://gitlab.com/whypaper/hofemfealpy.git # 
git clone https://gitlab.com/whypaper/headaptive.git # 半边数据结构
git clone https://gitlab.com/whypaper/avemfriction.git # 王飞
git clone https://gitlab.com/whypaper/havem.git # 杂交虚单元误差估计
git clone https://gitlab.com/whypaper/logmarking.git # 新型标记策略，蒋凯
git clone https://gitlab.com/whypaper/sfcavem.git # 王飞
git clone https://gitlab.com/whypaper/wg.git # 冯民富
git clone https://gitlab.com/whypaper/stokes-vem.git # stokes 虚单元
