#! /bin/bash
#
#

#mkdir /home/why/git
#mkdir /home/why/git/paper
#mkdir /home/why/git/students
#
#mkdir /home/why/.local/lib
#mkdir /home/why/.local/lib/python3.8
#mkdir /home/why/.local/lib/python3.8/dist-packages
#mkdir /home/why/.local/lib/python3.8/site-packages
#
## 系统内核更新
#sudo apt update
#sudo apt -y dist-upgrade 
#
## 版本控制
#sudo apt -y install curl
#sudo apt -y install git gitk  
#
## 安装中文支持
#sudo apt -y install language-pack-zh-hans 
#
## 安装中文输入法
#sudo apt -y install fcitx fcitx-table fcitx-pinyin

#  五笔输入法
if [ ! -d "~/.config/fcitx" ];then
    mkdir ~/.config/fcitx
    mkdir ~/.config/fcitx/table
elif [ ! -d "~/.config/fcitx/table" ];then
    mkdir ~/.config/fcitx/table
fi

cd ~/.config/fcitx/table
ln -s /home/why/git/tool/fcitx/table/wbx.conf wbx.conf
ln -s /home/why/git/tool/fcitx/table/wbx.mb wbx.mb

cd ~/
echo "
export fealpy=/home/why/git/fealpy
export site=/home/why/git/site
export tool=/home/why/git/tool
export numpde=/home/why/git/numpde
" >> .bashrc


#cd ~ # 进入主目录
#git clone https://github.com/weihuayi/Library.git

# 进入 git 目录
cd /home/why/git
# vim 
git clone --recurse-submodules https://github.com/weihuayi/vim.git
cd vim 
chmod +x install
./install

cd /home/why/git
git clone https://gitlab.com/weihuayi/fealpy.git


cd /home/why/git/
git clone https://gitlab.com/whygroup/numpde.git

cd /home/why/git/
git clone https://github.com/weihuayi/whysite.git site
git clone https://github.com/weihuayi/weihuayi.github.io.git

cd /home/why/git/paper
git clone https://gitlab.com/whypaper/quadadaptive.git
git clone https://gitlab.com/whypaper/hofemfealpy.git
git clone https://gitlab.com/whypaper/headaptive.git
git clone https://gitlab.com/whypaper/avemfriction.git
git clone https://gitlab.com/whypaper/havem.git
git clone https://gitlab.com/whypaper/logmarking.git
git clone https://gitlab.com/whypaper/sfcavem.git
git clone https://gitlab.com/whypaper/wg.git
git clone https://gitlab.com/whypaper/stokes-vem.git

cd /home/why/git/students
git clone https://gitlab.com/whygroup/students/caohuihui-knowledge.git
git clone https://gitlab.com/whygroup/students/wangpengxiang-knowledge.git
git clone https://gitlab.com/whygroup/students/wangdong-knowledge.git
git clone https://gitlab.com/whygroup/students/wangxin-knowledge.git
git clone https://gitlab.com/whygroup/students/lichengxin-knowledge.git
git clone https://gitlab.com/whygroup/students/gongxin-knowledge.git
git clone https://gitlab.com/whygroup/students/huhandan-knowledge.git
git clone https://gitlab.com/whygroup/students/liumeiyun-knowledge.git
git clone https://gitlab.com/whygroup/students/liujun-knowledge.git
git clone https://gitlab.com/whygroup/students/tiantian-knowledge.git
git clone https://gitlab.com/whygroup/students/zhoutiejun-knowledge.git
git clone https://gitlab.com/whygroup/students/chenchunyu-knowledge.git
git clone https://gitlab.com/whygroup/students/liujianggang-knowledge.git
git clone https://gitlab.com/whygroup/students/liao-knowledge.git


# git clone https://gitlab.com/whygroup/students/
# git clone https://github.com/weihuayi/whysc.git
# git clone https://github.com/weihuayi/numopde.git
# git clone https://github.com/weihuayi/numal.git
# git clone https://github.com/weihuayi/iwrite.git
#git clone --recursive https://github.com/weihuayi/package.git
