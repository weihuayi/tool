
apt -y install net-tools

apt -y install fcitx fcitx-table # 这是增加自己的 五笔输入法的必须
apt -y install fcitx5 fcitx5-table 

# 远程登录工具openssh-server
apt -y install openssh-server

# 程序开发工具 
## fortran编译器gfortran 
apt -y install  gfortran fort77  
## 软件构建管理工具
apt -y install build-essential automake cmake cmake-qt-gui 
## python3 相关
apt -y install python3-dev python3-pip 
apt -y install python3-rope python3-flake8
apt -y install python3-dbg 
apt -y install python3-tk
apt -y install pybind11-dev
apt -y install python3-importlib-metadata

# 编辑器
apt -y install vim vim-gtk
apt -y install xdotool 
apt -y install zathura

 
# 文档编译器texlive
apt -y install texlive-full texlive-bibtex-extra
apt -y install latexmk

# 并行库openmpi
apt -y install openmpi-bin openmpi-common openmpi-doc libopenmpi-dev

# gitbook 
apt -y install npm
# npm cache clean -f
# sudo npm install -g n
# sudo n stable
# upgrade to latest version
# sudo n latest

# Fix PATH
# sudo apt-get install --reinstall nodejs-legacy # fix /usr/bin/node

# To undo
# sudo n rm 6.0.0 # replace number with version of Node that was installed
# sudo npm uninstall -g n

npm -g install gitbook-cli

# zoom 
# zoom also depends ibus
apt -y install libegl1-mesa libgl1-mesa-glx libxcb-xtest0

# 科学计算专业软件
apt -y install metis
apt -y install libmumps-dev
apt -y install libgmp-dev libmpfr-dev 
apt -y install zlib1g-dev libblas-dev liblapack-dev
apt -y install libboost-dev libboost-thread-dev libboost-system-dev 
apt -y install cheese
